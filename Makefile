EXPIRY_CHECK := $(shell date -d'9 months' +%s)

KEYSERVER := hkps://keyring.devuan.org

GPG_OPTIONS := --homedir ./keyrings/gnupg --no-options --no-default-keyring --no-auto-check-trustdb

INDIVIDUAL_KEYS := $(wildcard public_keys/individual/*.gpg)
ARCHIVE_KEYS := $(wildcard public_keys/archive/*.gpg)
REMOVED_KEYS := $(wildcard public_keys/removed/*.gpg)

all: keyrings/devuan-archive-keyring.gpg keyrings/devuan-keyring.gpg keyrings/devuan-removed-keys.gpg

.DELETE_ON_ERROR:

keyrings/gnupg:
	install -m700 -d $@

define import-keys =
	cat $^ | gpg --no-keyring --import-options import-export --import > $@
endef

define check-expiry =
	for k in $$(gpg --no-keyring --with-colons --fixed-list-mode --show-keys $@ | grep -e '^pub' -e '^sub'); do \
	    expiry=$$(echo $$k | cut -d: -f7) ; \
	    if [ -n "$${expiry}" ] && [ $${expiry} -lt $(EXPIRY_CHECK) ] ; then \
	        echo ERROR: $$(echo $$k | cut -d: -f5) expires too soon: $$(date -d@$${expiry}) ; \
	        exit 1 ; \
	    fi ; \
	done
endef

keyrings/devuan-keyring.gpg: $(INDIVIDUAL_KEYS) | keyrings/gnupg
	$(import-keys)
	$(check-expiry)

keyrings/devuan-archive-keyring.gpg: $(ARCHIVE_KEYS) | keyrings/gnupg
	$(import-keys)
	$(check-expiry)

keyrings/devuan-removed-keys.gpg: $(REMOVED_KEYS) | keyrings/gnupg
	$(import-keys)

refresh: | keyrings/gnupg
	for k in $(INDIVIDUAL_KEYS) $(ARCHIVE_KEYS); do \
	    gpg $(GPG_OPTIONS) --keyserver $(KEYSERVER) --keyserver-options no-self-sigs-only --refresh-keys --keyring ./$$k ; \
	done

clean:
	rm -fr keyrings

.PHONY: clean refresh
